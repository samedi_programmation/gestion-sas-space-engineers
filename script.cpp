private IMyDoor lastOpenedDoor;

public Program()
{
    Runtime.UpdateFrequency = UpdateFrequency.Update100;
}

public void Main(string argument, UpdateType updateSource)
{

// Bout de script qui ferme automatiquement toute porte ouverte de la structure

//    List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
//    GridTerminalSystem.GetBlocks(blocks);
//
//    foreach (var block in blocks)
//    {
//        if (block is IMyDoor)
//        {
//            IMyDoor porte = (IMyDoor) block;
//            if (porte.Status.ToString() == "Open") {
//                porte.CloseDoor();
//            }
//        }
//
//    }


    IMyDoor DoorInterne = GridTerminalSystem.GetBlockWithName("SAS_2") as IMyDoor;
    IMySensorBlock sensorInterne = GridTerminalSystem.GetBlockWithName("SAS_Capteur_2") as IMySensorBlock;
    List<MyDetectedEntityInfo> detectedInterne = new List<MyDetectedEntityInfo>();
    sensorInterne.DetectedEntities(detectedInterne);

    IMyDoor DoorExterne = GridTerminalSystem.GetBlockWithName("SAS_1") as IMyDoor;
    IMySensorBlock sensorExterne = GridTerminalSystem.GetBlockWithName("SAS_Capteur_1") as IMySensorBlock;
    List<MyDetectedEntityInfo> detectedExterne = new List<MyDetectedEntityInfo>();
    sensorExterne.DetectedEntities(detectedExterne);

    IMySensorBlock sensorDedans = GridTerminalSystem.GetBlockWithName("SAS_Capteur_Interne") as IMySensorBlock;
    List<MyDetectedEntityInfo> detectedDedans = new List<MyDetectedEntityInfo>();
    sensorDedans.DetectedEntities(detectedDedans);


    IMyTextPanel EcranSamediProg = GridTerminalSystem.GetBlockWithName("Samedi_Prog_1") as IMyTextPanel;

    EcranSamediProg.WritePublicText("\nPorte Interne : " + DoorInterne.Status, false);
    EcranSamediProg.WritePublicText("\nDétectés Interne : " + detectedInterne.Count, true);
    EcranSamediProg.WritePublicText("\nPorte Externe : " + DoorExterne.Status, true);
    EcranSamediProg.WritePublicText("\nDétectés Externe : " + detectedExterne.Count, true);

    if (DoorExterne.Status.ToString() == "Open" || DoorExterne.Status.ToString() == "Opening") {
        lastOpenedDoor = DoorExterne;
    }
    if (DoorInterne.Status.ToString() == "Open" || DoorInterne.Status.ToString() == "Opening") {
        lastOpenedDoor = DoorInterne;
    }

    if (detectedInterne.Count > 0) {

        foreach(var qui in detectedInterne) {
            EcranSamediProg.WritePublicText("\nDétecté Interne : " + qui.Relationship.ToString() + " (" + qui.Name + ")", true);
        }

        DoorInterne.OpenDoor();
    } else if (DoorInterne.Status.ToString() != "Closed") {
        DoorInterne.CloseDoor();
    }
    if (detectedExterne.Count > 0) {

        foreach(var qui in detectedExterne) {
            EcranSamediProg.WritePublicText("\nDétecté Externe : " + qui.Relationship.ToString() + " (" + qui.Name + ")", true);
        }


        DoorExterne.OpenDoor();
    } else if (DoorExterne.Status.ToString() != "Closed") {
        DoorExterne.CloseDoor();
    }

    if (DoorExterne.Status.ToString() == "Closed" && DoorInterne.Status.ToString() == "Closed" && detectedDedans.Count > 0) {
        if (lastOpenedDoor == DoorExterne) {
            DoorInterne.OpenDoor();
        } else if (lastOpenedDoor == DoorInterne) {
            DoorExterne.OpenDoor();
        }
    }

}
