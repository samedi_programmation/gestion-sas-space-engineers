############################
# Les Samedi Programmation #
############################


Samedi 19/09/2020 : Scripting Space Engineers
=============================================


Ce code source est la base du live sur la gestion des SAS dans Space Engineers.


Il montre un exemple de gestion de capteurs pour ouvrir / fermer des portes de SAS, gérées par un script.

- Récupération d'objets dans le script
- Interactions
- Parcours de liste pour suivre l'état des entités détectées (proprio, faction, amis, ennemis, neutres)
- Ouverture de la porte opposée après entrée, une fois que la porte d'entrée est fermée


Assistance au scripting :

https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts

https://github.com/malware-dev/MDK-SE/wiki/Api-Index


Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex